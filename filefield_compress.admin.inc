<?php
// $Id$

/**
 * @file
 * Administration pages for FileField Compress module.
 */

/**
 * System settings form.
 */
function filefield_compress_settings() {
  $form = array();
  $form['filefield_compress_zip_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip binary location'),
    '#description' => t('Full path to the zip binary on the server. e.g.) <em>/usr/bin/zip</em>'),
    '#default_value' => variable_get('filefield_compress_zip_path', ''),
  );
  $form['filefield_compress_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug mode'),
    '#description' => t('When debugging is enabled the module will log extra information to the watchdog table.'),
    '#default_value' => variable_get('filefield_compress_debug', 0),
  );
  return system_settings_form($form);
}
